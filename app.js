require('dotenv').config();
const express = require('express')
const app = express()
const cors = require('cors')
const swaggerUi = require('swagger-ui-express');
const path = require('path')

const swaggerDocument = require('./swagger.json');
const apiRouter = require('./server/routes')
const errorHandler = require('./server/middlewares/errorHandler')
const PORT = process.env.PORT || 4000

// middlewares
app.use(cors())
app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(express.static(path.resolve(__dirname, 'client', 'dist')));
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

/**
 * @Routes /api
 * entrypoint for all API routes
*/
app.use("/api", apiRouter)

// redirect to react app
app.use((req, res) => {
  res.sendFile(path.resolve(__dirname, 'client', 'dist') + '/index.html');
})

app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`Listening on http://localhost:${PORT}`)
})