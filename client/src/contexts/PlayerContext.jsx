import { createContext, useState, useContext } from "react";

const PlayerContext = createContext();

export const PlayerContextProvider = ({ children }) => {
  const [players, setPlayers] = useState([
    {
      id: 1,
      username: "John",
      email: "john@app.com",
      password: "password",
      exp: 10,
      lvl: 1,
    },
    {
      id: 2,
      username: "Saputra",
      email: "saputra@app.com",
      password: "password",
      exp: 100,
      lvl: 2
    },
    {
      id: 3,
      username: "Lestaria",
      email: "lestaria@app.com",
      password: "password",
      exp: 10,
      lvl: 1
    },
  ]);

  return (
    <PlayerContext.Provider value={[players, setPlayers]}>
      {children}
    </PlayerContext.Provider>
  );
};

export const usePlayerContext = () => {
    return useContext(PlayerContext);
}