import { Navbar,Nav } from "react-bootstrap";
import { Container } from "react-bootstrap";
import { Link, useLocation } from "react-router-dom";

function Layout({ children }) {
  const location = useLocation();
  
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Link to={'/'} className="navbar-brand">Home</Link>
          <Nav className="mr-auto">
            <Link to={"/player"} className={location.pathname == '/player' ? 'nav-link active' : 'nav-link'}>
              Player
            </Link>
          </Nav>
        </Container>
      </Navbar>
      <section>
        <Container>
          {children}
        </Container>
      </section>
    </>
  );
}

export default Layout;
