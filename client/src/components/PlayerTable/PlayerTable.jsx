import React from "react";
import { Table, Button } from "react-bootstrap";
import { FaUserEdit, FaRegTimesCircle } from "react-icons/fa";

function PlayerTable({ players, handleShow, setPlayer }) {
  return (
    <Table striped bordered hover size="sm">
      <thead>
        <tr>
          <th>Id</th>
          <th>Username</th>
          <th>Email</th>
          <th>Exp</th>
          <th>Lvl</th>
          <th style={{width: 80}}>Action</th>
        </tr>
      </thead>
      <tbody>
        {players.map((player) => {
          return (
            <tr key={player.id}>
              <td>{player.id}</td>
              <td>{player.username}</td>
              <td>{player.email}</td>
              <td>{player.exp}</td>
              <td>{player.lvl}</td>
              <td>
                <Button variant="info" size="sm" title="Edit" className="mr-1" onClick={() => {
                  setPlayer(() => player);
                  handleShow('edit');
                }}>
                  <FaUserEdit />
                </Button>
                <Button variant="danger" size="sm" title="Delete" onClick={() => {
                  setPlayer(() => player);
                  handleShow('delete');
                }}>
                  <FaRegTimesCircle />
                </Button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
}

export default PlayerTable;
