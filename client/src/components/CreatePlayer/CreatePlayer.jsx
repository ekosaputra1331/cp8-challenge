import { Button, Modal, Form } from "react-bootstrap";

function CreatePlayer({ show, handleClose, createHandler, playerData}) {
  const { player, setPlayer } = playerData;

  return (
    <>
      <Modal show={show} size="md" onHide={() => handleClose("create")}>
        <Form onSubmit={createHandler}>
          <Modal.Header closeButton>
            <Modal.Title className="h5">Create New Player</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="username">
              <Form.Label className="mb-0">Username : </Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter username"
                required
                value={player.username}
                size="sm"
                onChange={(e) => {
                  setPlayer((prev) => {
                    return {
                      ...prev,
                      username: e.target.value,
                    };
                  });
                }}
              />
            </Form.Group>

            <Form.Group controlId="email">
              <Form.Label className="mb-0 mt-1">Email : </Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                required
                value={player.email}
                size="sm"
                onChange={(e) => {
                  setPlayer((prev) => {
                    return {
                      ...prev,
                      email: e.target.value,
                    };
                  });
                }}
              />
            </Form.Group>

            <Form.Group controlId="password">
              <Form.Label className="mb-0 mt-1">Password : </Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                required
                value={player.password}
                size="sm"
                autoComplete="off"
                onChange={(e) => {
                  setPlayer((prev) => {
                    return {
                      ...prev,
                      password: e.target.value,
                    };
                  });
                }}
              />
            </Form.Group>

            <Form.Group controlId="exp">
              <Form.Label className="mb-0 mt-1">Experience : </Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter exp"
                required
                value={player.exp}
                min={0}
                size="sm"
                onChange={(e) => {
                  setPlayer((prev) => {
                    return {
                      ...prev,
                      exp: e.target.value,
                    };
                  });
                }}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => handleClose("create")}>
              Close
            </Button>
            <Button variant="primary" type="submit">
              Save
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}

export default CreatePlayer;
