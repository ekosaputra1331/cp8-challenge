import { Form, InputGroup, FormControl, Button } from "react-bootstrap";

function SearchPlayer({ searchHanlder, search }) {
  const [formData, setFormData] = search;

  

  return (
    <>
      <Form onSubmit={searchHanlder}>
        <InputGroup className="mb-3" size="sm">
          <Form.Control
            as="select"
            required
            value={formData.category}
            onChange={(e) => {
              setFormData((prev) => {
                return {
                  ...prev,
                  category: e.target.value,
                };
              });
            }}
          >
            <option value="username">Username</option>
            <option value="email">Email</option>
            <option value="exp">Experince</option>
            <option value="lvl">Level</option>
          </Form.Control>
          <FormControl
            type="text"
            placeholder="Search term"
            required
            style={{ width: "60%" }}
            value={formData.search}
            onChange={(e) => {
              setFormData((prev) => {
                return {
                  ...prev,
                  search: e.target.value,
                };
              });
            }}
          />
          <Button type="submit" variant="primary" size="sm">
            Search
          </Button>
        </InputGroup>
      </Form>

      <h5>Result : </h5>
    </>
  );
}

export default SearchPlayer;
