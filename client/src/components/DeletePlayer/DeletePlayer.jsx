import { Button, Modal, Form } from "react-bootstrap";

function DeletePlayer({ show, handleClose, deleteHandler, playerData }) {
  const { player } = playerData;

  return (
    <>
      <Modal show={show} size="md" onHide={() => handleClose("delete")}>
        <Form onSubmit={deleteHandler}>
          <Modal.Header closeButton>
            <Modal.Title className="h5">Delete Player</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Are you sure you want to delete <b>{player.username}</b> ?
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => handleClose("delete")}>
              Close
            </Button>
            <Button variant="danger" type="submit">
              Delete
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}

export default DeletePlayer;
