import "./App.css";
import Layout from "./layouts/Layout";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Player from "./pages/Player";
import NotFound from "./pages/NotFound";

function App() {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/player" element={<Player />} />
          <Route path="*" element={<NotFound />}/>
        </Routes>
        {/* Add more routes for additional pages */}
      </Layout>
    </Router>
  );
}

export default App;
