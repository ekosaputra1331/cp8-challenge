import { Button } from "react-bootstrap";
import PlayerTable from "../components/PlayerTable/PlayerTable";
import { usePlayerContext } from "../contexts/PlayerContext";
import { useState } from "react";
import CreatePlayer from "../components/CreatePlayer/CreatePlayer";
import { toast } from "react-toastify";
import EditPlayer from "../components/EditPlayer/EditPlayer";
import DeletePlayer from "../components/DeletePlayer/DeletePlayer";

function Home() {
  const [players, setPlayers] = usePlayerContext();

  const [player, setPlayer] = useState({
    username: "",
    password: "",
    email: "",
    exp: 0,
  });

  const [show, setShow] = useState({
    create: false,
    edit: false,
    delete: false,
  });

  const handleShow = (modal) => {
    setShow((prev) => {
      prev[modal] = true;
      return {
        ...prev,
      };
    });
  };

  const handleClose = (modal) => {
    setShow((prev) => {
      prev[modal] = false;
      return {
        ...prev,
      };
    });

    setPlayer(() => {
      return {
        username: "",
        password: "",
        email: "",
        exp: 0,
      };
    });
  };

  const createHandler = (e) => {
    e.preventDefault();
    if (player.username === "" || player.password === "" || player.email === "")
      return toast.error("Please fill all fields", {
        position: "top-right",
        autoClose: 1500,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "light",
      });

    if (players.find((item) => item.username === player.username)) {
      return toast.error(player.username + " already exists", {
        position: "top-right",
        autoClose: 1500,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "light",
      });
    }

    // creating new player
    player.id = players[players.length - 1].id + 1;
    player.lvl = 1;
    handleClose("create");
    setPlayers((prev) => {
      return [...prev, player];
    });
    setPlayer(() => {
      return {
        username: "",
        password: "",
        email: "",
        exp: 0,
      };
    });

    return toast.success("Player added successfully", {
      position: "top-right",
      autoClose: 1500,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: false,
      progress: undefined,
      theme: "light",
    });
  };

  const editHandler = (e) => {
    e.preventDefault();
    if (player.username === "" || player.password === "" || player.email === "")
      return toast.error("Please fill all fields", {
        position: "top-right",
        autoClose: 1500,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "light",
      });

    handleClose("edit");

    // updating the player
    setPlayers((prev) => {
      const newPlayers = prev.map((item) => {
        if (item.id === player.id) {
          return player;
        }
        return item;
      });
      return [...newPlayers];
    });

    toast.success(player.username + " has been updated successfully", {
      position: "top-right",
      autoClose: 1500,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: false,
      progress: undefined,
      theme: "light",
    });

    setPlayer(() => {
      return {
        username: "",
        password: "",
        email: "",
        exp: 0,
      };
    });
  };

  const deleteHandler = (e) => {
    e.preventDefault();
    if (!player.id)
      return toast.error("Please fill all fields", {
        position: "top-right",
        autoClose: 1500,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "light",
      });

    handleClose("delete");

    // delete the player
    setPlayers((prev) => {
      const newPlayers = prev.filter((item) => item.id !== player.id);
      return [...newPlayers];
    });

    toast.info(player.username + " has been deleted successfully", {
      position: "top-right",
      autoClose: 1500,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: false,
      progress: undefined,
      theme: "light",
    });

    setPlayer(() => {
      return {
        username: "",
        password: "",
        email: "",
        exp: 0,
      };
    });
  };

  return (
    <>
      <h2 className="text-center mt-3">Welcome</h2>
      <h5>Players Overview</h5>
      <PlayerTable
        players={players}
        handleShow={handleShow}
        setPlayer={setPlayer}
      />
      <Button variant="primary" size="sm" onClick={() => handleShow("create")}>
        New Player
      </Button>
      <CreatePlayer
        show={show.create}
        handleClose={handleClose}
        createHandler={createHandler}
        playerData={{ player, setPlayer }}
      />
      <EditPlayer
        show={show.edit}
        handleClose={handleClose}
        editHandler={editHandler}
        playerData={{ player, setPlayer }}
      />
      <DeletePlayer
        show={show.delete}
        handleClose={handleClose}
        deleteHandler={deleteHandler}
        playerData={{ player, setPlayer }}
      />
    </>
  );
}

export default Home;
