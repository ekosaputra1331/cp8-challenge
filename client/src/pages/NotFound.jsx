function NotFound() {
  return (
    <div className="text-center mt-5 font-weight-bolder">404 Not Found</div>
  )
}

export default NotFound