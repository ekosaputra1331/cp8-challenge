import { useState, useEffect } from "react";
import SearchPlayer from "../components/SearchPlayer/SearchPlayer";
import { usePlayerContext } from "../contexts/PlayerContext";
import { Table } from "react-bootstrap";

function Player() {
  const [players] = usePlayerContext();
  const [result, setResult] = useState(players);
  const [formData, setFormData] = useState({
    category: "username",
    search: "",
  });

  useEffect(() => {
    if (formData.search === "") {
      resetResult();
    }
    searchHanlder();
  }, [formData]);

  const searchHanlder = (e) => {
    if (typeof e !== "undefined") {
      e.preventDefault();
    }

    const results = players.filter((item) => {
      return item[formData.category]
        .toString()
        .toLowerCase()
        .includes(formData.search.toLowerCase());
    });

    setResult(() => results);
  };

  const resetResult = () => {
    setResult(() => players);
  };

  return (
    <>
      <h5 className="mt-2">Search Player</h5>
      <SearchPlayer
        searchHanlder={searchHanlder}
        search={[formData, setFormData]}
      />
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>Id</th>
            <th>Username</th>
            <th>Email</th>
            <th>Exp</th>
            <th>Lvl</th>
          </tr>
        </thead>
        <tbody>
          {result.map((player) => {
            return (
              <tr key={player.id}>
                <td>{player.id}</td>
                <td>{player.username}</td>
                <td>{player.email}</td>
                <td>{player.exp}</td>
                <td>{player.lvl}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </>
  );
}

export default Player;
